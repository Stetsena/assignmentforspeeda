# Rss Aggregator
Implemented and ready for use

### Prerequisites

Please make sure you have latest SBT installed on your environment!


### Implementation Details

Solution Using Akka framework.

- Every RSS stream will be processed as a separate Actor (Process)
- If any of the Rss Feeds will have errors in parsing, it will be skipped from result, error will be displayed in logs.
- Solution aggregate all the feeds in one output stream, containing all processed entities

### Running

When you have your SBT installed please in terminal open Project folder
and execute next command to see help menu appeared:

```
sbt "run --help"

```

You should see following menu:

```
RssConverter 1.0
Usage: RssConverter [options]

  -m, --mapping url1=conv1:conv2,url2=conv1:conv2...
                           Rss Url to Converter rules mapping
  -f, --fileName fileName  Name of the file which will store Rss Logs
Program terminated
```


Now in order to run application please run next example:

```
sbt "run --mapping http://tech.uzabase.com/rss=NewsPicksConverter:DoubleSpeedaConverter,http://feeds.abcnews.com/abcnews/topstories=NewsPicksConverter -f speedy_rss.log

```

You will see output in console and speedy_rss.log file will be created

*Note:
1) Application can handle more than one RSS source
2) Every RSS source can have custom converters assigned to it
3) File name is optional, if you do not provide it, you will just see standard output

### Testing

In order to run simple integration tests, please run following command:


```
sbt test

```