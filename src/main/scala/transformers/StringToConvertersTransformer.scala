package transformers

import convertors.{Converter, ConvertersHolder}

/**
  * Created by andrew on 4/30/17.
  */
object StringToConvertersTransformer {
  implicit class StringToConverters(convertersString : String) {
     def asConvertersList() : List[Converter] ={
      convertersString.split("[:]").map( name => {
        ConvertersHolder.getConverter(name)
      }).toList
    }
  }
}
