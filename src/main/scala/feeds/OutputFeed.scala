package feeds

import java.util

import com.sun.syndication.feed.synd.{SyndEntryImpl, SyndFeedImpl}
import scala.collection.JavaConverters._

/**
  * Created by andrew on 4/29/17.
  *
  */
object OutputFeed{
  def apply(entryList: List[SyndEntryImpl]): OutputFeed = new OutputFeed(entryList)
}

class OutputFeed(entryList : List[SyndEntryImpl]) {
  val feed = new SyndFeedImpl
  feed.setFeedType("rss_2.0")
  feed.setTitle("Aggregated Feed")
  feed.setDescription("SPEEDA Aggregated Feed")
  feed.setAuthor("Andrew Stetsenko")
  feed.setLink("http://www.andrew.com")
  feed.setEntries(entryList.asJava)
}
