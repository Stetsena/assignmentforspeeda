package convertors

import com.sun.syndication.feed.synd.SyndEntryImpl

/**
  * Created by andrew on 4/29/17.
  */
trait Converter {
  def convert(entry : SyndEntryImpl): SyndEntryImpl
}


