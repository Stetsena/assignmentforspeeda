package convertors

/**
  * Created by andrew on 4/30/17.
  */
object ConvertersHolder {

  val REGEX = "([N])([eE])([Ww])([Ss])([Pp])([Ii])([cC])([Kk])([Ss])"
  val SPEEDA = "SPEEDA"

  val newsPicksConverter: Converter = (entry) => {
    entry.setTitle(entry.getTitle.replaceAll(REGEX,SPEEDA))
    entry.getDescription.setValue(entry.getDescription.getValue.replaceAll(REGEX,SPEEDA))
    entry
  }

  val doubleSpeedaConverter: Converter = (entry) => {
    entry.setTitle(entry.getTitle.replaceAll("SPEEDA/SPEEDA",SPEEDA))
    entry.getDescription.setValue(entry.getDescription.getValue.replaceAll("SPEEDA/SPEEDA",SPEEDA))
    entry
  }

  val activeConverters: Map[String, Converter]= Map("NewsPicksConverter"    -> newsPicksConverter,
                                                    "DoubleSpeedaConverter" -> doubleSpeedaConverter)
  def getConverter(converterName: String) : Converter ={
    activeConverters(converterName)
  }

  def getActiveConvertersString() : String ={
    activeConverters.keySet.mkString(", ")
  }

}
