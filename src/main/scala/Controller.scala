/**
  * Created by andrew on 4/29/17.
  */


import actors.RssFeedRouter
import com.sun.syndication.feed.synd.SyndFeed
import config.Config
import convertors.{Converter, ConvertersHolder}
import requests.FeedProcessRequest
import validators.{ConvertersValidator, UrlsValidator}

object Controller {
  def main(args: Array[String]): Unit = {

    val parser = new scopt.OptionParser[Config]("RssConverter") {
      head("RssConverter", "1.0")

      opt[Map[String, String]]('m', "mapping").required()
        .valueName("url1=conv1:conv2,url2=conv1:conv2...")
        .validate(x => {
          if (!UrlsValidator.isValidUrl(x.keySet)) {
            failure("ERROR: All Url should be valid rss URL's")
          }
          else if (!ConvertersValidator.isValidConverters(x.values)) {
            failure("ERROR: All Converters should be existing converter names,"
              +" specified in format conv1:conv2..."
              +" Existing Converters -> "
              + ConvertersHolder.getActiveConvertersString())
          }
          else success
        })
        .action((x, c) => c.copy(rssUrlConverterMapping = x))
        .text("Rss Url to Converter rules mapping")

      opt[String]('f', "fileName").optional()
        .valueName("fileName")
        .action( (x, c) => c.copy(fileName = x))
        .text("Name of the file which will store Rss Logs")
    }

    parser.parse(args, Config()) match {
      case Some(config) =>
        val router = RssFeedRouter(config.rssUrlConverterMapping.size, config.fileName)
        config.rssUrlConverterMapping.foreach(param => {
          router.tell(FeedProcessRequest(param._1, param._2))
        })
      case None =>
        println("Program terminated")
    }

  }
}
