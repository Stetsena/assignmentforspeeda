package validators

import convertors.{Converter, ConvertersHolder}


/**
  * Created by andrew on 4/30/17.
  */


object ConvertersValidator {

  def isValidConverter(convertersString : String): Boolean = {
    ConvertersHolder.activeConverters.keySet.contains(convertersString)
  }

  def isValidConverters(convertersStringList : Iterable[String]): Boolean = {
    convertersStringList.forall(converterRaw => {
      converterRaw.split("[:]").forall(isValidConverter(_))
    })
  }



}
