package validators

/**
  * Created by andrew on 4/30/17.
  */

import org.apache.commons.validator.routines.UrlValidator

object UrlsValidator {

  def isValidUrl(urlString: String): Boolean = {
    val schemes = Array("http", "https")
    val urlValidator = new UrlValidator(schemes)
    if (urlValidator.isValid(urlString)) true else false
  }

  def isValidUrl(urlStringSeq: Set[String]): Boolean = {
    urlStringSeq.forall(isValidUrl(_))
  }

}
