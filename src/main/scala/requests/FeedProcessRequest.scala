package requests

import com.sun.syndication.feed.synd.{SyndEntryImpl, SyndFeed}
import convertors.Converter

/**
  * Created by andrew on 4/29/17.
  */
trait Request

case class FeedProcessRequest(feed : String, converters : String) extends Request

case class FeedProcessedRequest(entryList : List[SyndEntryImpl]) extends Request

