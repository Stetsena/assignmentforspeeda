package config

/**
  * Created by andrew on 4/30/17.
  */
case class Config(rssUrlConverterMapping: Map[String,String] = Map(), fileName : String = "")