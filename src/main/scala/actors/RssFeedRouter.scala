package actors

/**
  * Created by andrew on 4/29/17.
  */
import akka.actor.{ActorSystem, Props}
import akka.routing.RoundRobinPool
import requests.FeedProcessRequest

object RssFeedRouter {
  def apply(numberOfFeeds: Int, outputFile: String): RssFeedRouter = new RssFeedRouter(numberOfFeeds, outputFile)
}

class RssFeedRouter( numberOfFeeds: Int , outputFile: String) {

  private val system = ActorSystem.create("system")
  private val agregatorActor = system.actorOf(Props(classOf[FeedsAgregatorActor], numberOfFeeds, this, outputFile))
  private val router = system.actorOf(RoundRobinPool(numberOfFeeds).props(
    routeeProps = Props[FeedProcessingActor]))

  def tell(request : FeedProcessRequest) ={
    router.tell(request, agregatorActor)
  }

  def stopSystem ={
    system.terminate()
  }

}
