package actors

import java.io.{File, PrintWriter}

import akka.actor.{Actor, ActorLogging, ActorSystem}
import com.sun.syndication.feed.synd.SyndEntryImpl
import com.sun.syndication.io.SyndFeedOutput
import feeds.OutputFeed
import requests.FeedProcessedRequest

/**
  * Created by andrew on 4/30/17.
  */
class FeedsAgregatorActor(feedsToProcess: Int, rssFeedRouter: RssFeedRouter, outputFile : String) extends Actor with ActorLogging {

  var agregatedEntries: List[SyndEntryImpl] = List()
  var feedsLeft = feedsToProcess

  def receive = {
    case FeedProcessedRequest(entries) => {
        addCollectedEntries(entries)
        decreaseUnprocessedFeedsNumber
        if(allFeedsProcessed){
          printAgregatedFeed(outputFile)
          rssFeedRouter.stopSystem
        }
    }
    case _ => log.info("Unknown message received!")
  }

  private def addCollectedEntries(entries: List[SyndEntryImpl]) = {
    agregatedEntries = agregatedEntries ::: entries

  }

  private def decreaseUnprocessedFeedsNumber = {
    feedsLeft -= 1
  }

  private def printAgregatedFeed(fileName: String) = {
    val outFeed = OutputFeed(agregatedEntries)
    val output = new SyndFeedOutput
    output.output(outFeed.feed, new PrintWriter(System.out))
    if(!fileName.isEmpty) output.output(outFeed.feed, new PrintWriter(new File(fileName)))
  }

  private def allFeedsProcessed = {
    feedsLeft == 0
  }
}
