package actors

import java.net.URL

import akka.actor.{Actor, ActorLogging}
import com.sun.syndication.feed.synd.{SyndEntryImpl, SyndFeed}
import com.sun.syndication.io.{SyndFeedInput, XmlReader}
import convertors.Converter
import requests.{FeedProcessRequest, FeedProcessedRequest}
import transformers.StringToConvertersTransformer._

/**
  * Created by andrew on 4/29/17.
  */
class FeedProcessingActor extends Actor with ActorLogging {

  def receive = {
    case FeedProcessRequest(feedUrl, converterList) => {
      var convertedEntries: List[SyndEntryImpl] = List()
      try {
        log.info(s"Generating feed for <$feedUrl>")
        val feed = getFeed(feedUrl)
        log.info(s"Processing feed <${feed.getTitle}>")
        getEntries(feed).map(currentEntry => {
          convertedEntries = convertedEntries :+ processEntry(converterList.asConvertersList(), currentEntry)
        })
        log.info(s"<${feed.getTitle}> feed entries processed")
      } catch {
        case e : Throwable => log.error(e, s"There is an Error in generating feed for <$feedUrl>, skipping this feed")
      }
      sender() ! FeedProcessedRequest(convertedEntries)
    }
    case _ => log.info("Unknown message received!")
  }


  private def getFeed(feedUrl: String) = {
    val feedInput = new SyndFeedInput()
    val feed = feedInput.build(new XmlReader(new URL(feedUrl)))
    feed
  }

  private def processEntry(convertors: List[Converter], entry: SyndEntryImpl): SyndEntryImpl = {
    var processingEntry = entry
    convertors.foreach(converter => {
      processingEntry = converter.convert(processingEntry)
    })
    processingEntry
  }

  private def getEntries(feed: SyndFeed) = {
    feed.getEntries.toArray(new Array[SyndEntryImpl](feed.getEntries.size()))
  }
}

