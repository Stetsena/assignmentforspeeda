package aktors

import actors.{FeedProcessingActor, FeedsAgregatorActor}
import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import requests.FeedProcessRequest



class FeedProcessorSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with Eventually{
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }


  "An FeedProcessingActor" must {


    "send back list of converter entities" in {
      val actor = TestActorRef[FeedsAgregatorActor](Props(classOf[FeedsAgregatorActor], 20, null, ""))
      val feedProcessor = system.actorOf(Props[FeedProcessingActor])
      feedProcessor.tell(FeedProcessRequest("http://tech.uzabase.com/rss", "NewsPicksConverter:DoubleSpeedaConverter"), actor)
      eventually (timeout(Span(30, Seconds)), interval(Span(30, Seconds))) {
        actor.underlyingActor.agregatedEntries.size should equal(30)
      }
    }

    "throw exception in log if have parsing problems and return 0 entities" in {
      val actor = TestActorRef[FeedsAgregatorActor](Props(classOf[FeedsAgregatorActor], 20, null, ""))
      val feedProcessor = system.actorOf(Props[FeedProcessingActor])
      feedProcessor.tell(FeedProcessRequest("http://tech.uzabase.cm/rss", "NewsPicksConverter:DoubleSpeedaConverter"), actor)
      eventually (timeout(Span(30, Seconds)), interval(Span(30, Seconds))) {
        actor.underlyingActor.agregatedEntries.size should equal(0)
      }
    }

  }

}
