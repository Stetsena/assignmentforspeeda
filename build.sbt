name := "RssAgregatorApp"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies += "rome" % "rome" % "1.0"
libraryDependencies += "com.typesafe.akka" % "akka-actor_2.12" % "2.5.0"
libraryDependencies += "com.github.scopt" % "scopt_2.12" % "3.5.0"
libraryDependencies += "commons-validator" % "commons-validator" % "1.6"
libraryDependencies += "com.typesafe.akka" % "akka-testkit_2.12" % "2.5.0"
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.2.0-SNAP6"